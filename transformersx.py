import os

import torch
from transformers import AutoModelForCausalLM, AutoTokenizer


class AutoModelForCausalLMWrapper:
    def __init__(
        self,
        model_name: str = os.environ.get("MODEL_NAME_OR_PATH", "lmsys/vicuna-13b-v1.5-16k"),
    ) -> None:
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.model = AutoModelForCausalLM.from_pretrained(
            model_name,
            device_map="auto",
            load_in_4bit=True,
        ).train(False)

    def __call__(
        self,
        prompt: str,
    ) -> str:
        inputs = self.tokenizer(
            [prompt],
            return_tensors="pt",
            add_special_tokens=False,
        ).to("cuda")
        with torch.no_grad():
            outputs = self.model.generate(
                **inputs,
                max_length=1024,
                max_new_tokens=128,
                do_sample=True,
                temperature=0.5,
                top_k=10,
                top_p=1.0,
            )[0]
        outputs_decoded = self.tokenizer.decode(outputs)

        return outputs_decoded


__all__ = [
    "AutoModelForCausalLMWrapper",
]


def __dir__():
    return __all__
