from vllm import LLM, SamplingParams


class VLLMWrapper:
    def __init__(
        self,
        model: str = "lmsys/vicuna-13b-v1.5-16k",
    ) -> None:
        self.llm = LLM(model=model)
        self.sampling_params = SamplingParams(
            temperature=0.5,
            top_p=0.95,
            top_k=10,
            max_tokens=512,
        )

    def __call__(
        self,
        prompt: str,
    ) -> str | None:
        try:
            outputs = self.llm.generate(prompts, self.sampling_params)

            return outputs[0].outputs[0].text

        except:
            return None


__all__ = [
    "VLLMWrapper",
]


def __dir__():
    return __all__
