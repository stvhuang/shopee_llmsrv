import flask

from transformersx import AutoModelForCausalLMWrapper
from vllmx import VLLMWrapper

llm = AutoModelForCausalLMWrapper()
# llm = VLLMWrapper()
app = flask.Flask(__name__)


@app.route("/chat", methods=["POST"])
def chat():
    prompt: dict = flask.request.get_json().get("prompt", None)

    response = {}

    if prompt:
        try:
            response["response"] = llm(str(prompt))
        except Exception as e:
            response["error"] = repr(e)
    else:
        response["error"] = "Empty prompt!"

    return response
