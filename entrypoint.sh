set -x

printenv

apt-get update
apt-get upgrade -y curl git rclone vim

pip install -U pip setuptools wheel
pip install -U flask requests
pip install -IU --no-deps uwsgi
pip install -U \
  bitsandbytes \
  git+https://github.com/huggingface/transformers \
  numpy \
  peft \
  protobuf==3.20.3 \
  scipy \
  sentencepiece \
  torch \
  vllm

uwsgi --processes 1 --master --lazy --module main:app --http 0.0.0.0:80
